<?php
// Add custom Theme Functions here
function tao_custom_post_type()
{
 
    /*
     * Biến $label để chứa các text liên quan đến tên hiển thị của Post Type trong Admin
     */
    $label = array(
        'name' => 'Menus', //Tên post type dạng số nhiều
        'singular_name' => 'menu' //Tên post type dạng số ít
    );
 
    /*
     * Biến $args là những tham số quan trọng trong Post Type
     */
    $args = array(
        'labels' => $label, //Gọi các label trong biến $label ở trên
        'description' => 'Post type đăng thực đơn', //Mô tả của post type
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ), //Các tính năng được hỗ trợ trong post type
        'taxonomies' => array( 'category', 'post_tag' ), //Các taxonomy được phép sử dụng để phân loại nội dung
        'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
        'public' => true, //Kích hoạt post type
        'show_ui' => true, //Hiển thị khung quản trị như Post/Page
        'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
        'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
        'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
        'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
        'menu_icon' => '', //Đường dẫn tới icon sẽ hiển thị
        'can_export' => true, //Có thể export nội dung bằng Tools -> Export
        'has_archive' => true, //Cho phép lưu trữ (month, date, year)
        'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
        'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
        'capability_type' => 'post' //
    );
 
    register_post_type('sanpham', $args); //Tạo post type với slug tên là sanpham và các tham số trong biến $args ở trên
 
}
/* Kích hoạt hàm tạo custom post type */
add_action('init', 'tao_custom_post_type');

add_filter('pre_get_posts','lay_custom_post_type');
function lay_custom_post_type($query) {
  if (is_home() && $query->is_main_query ())
    $query->set ('post_type', array ('post','sanpham'));
    return $query;
}

if( !function_exists( 'isn_bootstrap_pagination' ) ) {
    /**
     * [isn_bootstrap_pagination]
     * @author thanhnam <namtt@acro.vn> 
     */
    function isn_bootstrap_pagination( $query=null ) {
     
        global $wp_query;
        $query = $query ? $query : $wp_query;
        $big = 999999999;   
     
        $paginate = paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'type' => 'array',
                'total' => $query->max_num_pages,
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'prev_text' => '&laquo;',
                'next_text' => '&raquo;',
            )
        );
     
        if ($query->max_num_pages > 1) :
        ?>
            <ul class="pagination">
        <?php
                foreach ( $paginate as $page ) {
                    echo '<li>' . $page . '</li>';
                }
        ?>
            </ul>
        <?php
        endif;
    }
}

function get_job() {
    ob_start();

    $jobPostsArgs = array(
        'post_type' => 'sanpham',
        'posts_per_page' => 15,
        'paged' => 1,
        'order' => 'ASC',
        'orderby' => 'ID',
    );
    $jobPosts = new WP_Query( $jobPostsArgs );
    if( $jobPosts->have_posts() ) { 
        while( $jobPosts->have_posts() ) { 
            $jobPosts->the_post();
            ?>
                <div class="home-menu">
                    <div class="post-menu">
                        <a href="<?php the_permalink(); ?>">
                            <div class="image-menu"><?php the_post_thumbnail(); ?>  </div> 
                            <h3><?php echo the_title(); ?> </h3>
                            <?php the_excerpt(); ?>
                        </a>
                    </div>
                </div>            
            <?php
        }
    }
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($jobPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_job', 'get_job' );



function tao_custom_post_five_star()
{
 
    /*
     * Biến $label để chứa các text liên quan đến tên hiển thị của Post Type trong Admin
     */
    $label2 = array(
        'name' => 'Rate_five_stars', //Tên post type dạng số nhiều
        'singular_name' => 'Rate_five_stars' //Tên post type dạng số ít
    );
 
    /*
     * Biến $args là những tham số quan trọng trong Post Type
     */
    $args2 = array(
        'labels' => $label2, //Gọi các label trong biến $label ở trên
        'description' => 'Post type đăng thực đơn', //Mô tả của post type
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ), //Các tính năng được hỗ trợ trong post type
        'taxonomies' => array( 'category', 'post_tag' ), //Các taxonomy được phép sử dụng để phân loại nội dung
        'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
        'public' => true, //Kích hoạt post type
        'show_ui' => true, //Hiển thị khung quản trị như Post/Page
        'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
        'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
        'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
        'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
        'menu_icon' => '', //Đường dẫn tới icon sẽ hiển thị
        'can_export' => true, //Có thể export nội dung bằng Tools -> Export
        'has_archive' => true, //Cho phép lưu trữ (month, date, year)
        'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
        'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
        'capability_type' => 'post' //
    );
 
    register_post_type('five_star', $args2); //Tạo post type với slug tên là sanpham và các tham số trong biến $args ở trên
 
}
/* Kích hoạt hàm tạo custom post type */
add_action('init', 'tao_custom_post_five_star');


function get_five_star() {
    ob_start();

    $fivestarPostsArgs = array(
        'post_type' => 'five_star',
        'posts_per_page' => 15,
        'paged' => 1,
        'order' => 'ASC',
        'orderby' => 'ID',
    );
    $fivestarPosts = new WP_Query( $fivestarPostsArgs );
    if( $fivestarPosts->have_posts() ) { 
        while( $fivestarPosts->have_posts() ) { 
            $fivestarPosts->the_post();
            ?>
                    <div class="post-fs">
                        <a href="<?php the_permalink(); ?>">
                            <div class="image-fs"><?php the_post_thumbnail(); ?>  </div> 
                            <h3 class="h3-fs"><?php echo the_title(); ?> </h3>
                            <div class="excerpt-fs"> <?php the_excerpt(); ?> </div>
                            <button class="btn-fs">View More-></button>
                        </a>
                    </div> 
            <?php
        }
    }
    ?>
        <div class="text-center">
            <?php isn_bootstrap_pagination($fivestarPosts); ?>
        </div>
    <?php
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'get_five_star', 'get_five_star' );
